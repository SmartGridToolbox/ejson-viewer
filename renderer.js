// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

window.onload = function() {
    syncUseSprings();
    syncPinNodes();
    syncVLow();
    syncVHigh();
    syncPScale();
    syncSpringCoeff();
    syncRepulsion();
    syncDrag();
    syncScale();
};

function syncUseSprings() {
    let checkbox = document.getElementById('use-spring-layout');
    window.api.eNetworkViewer().setUseSprings(checkbox.checked);
}

function syncPinNodes() {
    let checkbox = document.getElementById('pin-nodes');
    window.api.eNetworkViewer().setPinNodes(checkbox.checked);
}

function syncShowHeatmap() {
    let checkbox = document.getElementById('show-heatmap');
    window.api.eNetworkViewer().setShowHeatmap(checkbox.checked);
}

function syncShowMap() {
    let checkbox = document.getElementById('show-map');
    window.api.eNetworkViewer().setShowMap(checkbox.checked);
}

function syncVLow() {
    let sliderLow = document.getElementById('range-v-low')
    let sliderHigh = document.getElementById('range-v-high')
    let labelLow = document.getElementById('label-v-low')
    vLow = Number(sliderLow.value);
    vHigh = Number(sliderHigh.value);
    if (vLow > vHigh) {
        vLow = vHigh;
        sliderLow.value = vLow;
    }
    labelLow.innerHTML = vLow.toFixed(3);
    window.api.eNetworkViewer().setVLow(vLow);
}

function syncVHigh() {
    let sliderLow = document.getElementById('range-v-low')
    let sliderHigh = document.getElementById('range-v-high')
    let labelHigh = document.getElementById('label-v-high')
    vLow = Number(sliderLow.value);
    vHigh = Number(sliderHigh.value);
    if (vLow > vHigh) {
        vHigh = vLow;
        sliderHigh.value = vHigh;
    }
    labelHigh.innerHTML = vHigh.toFixed(3);
    window.api.eNetworkViewer().setVHigh(vHigh);
}

function syncPScale() {
    let slider = document.getElementById('range-p-scale')
    let label = document.getElementById('label-p-scale')
    var val = Number(slider.value);
    label.innerHTML = val.toFixed(5);
    window.api.eNetworkViewer().setPScale(val);
}

function syncSpringCoeff() {
    let slider = document.getElementById('range-spring-coeff')
    let label = document.getElementById('label-spring-coeff')
    let val = 0.0025 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    window.api.eNetworkViewer().setSpringCoeff(val);
}

function syncRepulsion() {
    let slider = document.getElementById('range-repulsion')
    let label = document.getElementById('label-repulsion')
    let val = -2.0 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    window.api.eNetworkViewer().setRepulsion(val);
}

function syncDrag() {
    let slider = document.getElementById('range-drag')
    let label = document.getElementById('label-drag')
    let val = 0.1 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    window.api.eNetworkViewer().setDrag(val);
}

function syncScale() {
    let slider = document.getElementById('range-scale')
    let label = document.getElementById('label-scale')
    let val = 10.0 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    window.api.eNetworkViewer().setScale(val);
}

function search() {
    let searchBar = document.getElementById('search-field')
    window.api.eNetworkViewer().search(searchBar.value);
}

// vim: ts=4 sw=4:
