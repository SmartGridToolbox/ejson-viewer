// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
        const element = document.getElementById(selector);
        if (element) element.innerText = text;
    };

    for (const type of ['chrome', 'node', 'electron']) {
        replaceText(`${type}-version`, process.versions[type]);
    }
})

const electron = require('electron');
const {contextBridge, ipcRenderer} = electron;

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object

// KLUDGE: set the zoom factor here.
process.once('loaded', () => {
    electron.webFrame.setZoomFactor(1.0);
});

var eNetworkViewer = null;
window.onload = () => {
    const viewer = require('e-network-viewer');
    const networkDiv = document.getElementById('network'); 
    const propertiesDiv = document.getElementById('properties'); 
    eNetworkViewer = viewer.ENetworkViewer(networkDiv, propertiesDiv);
}

async function openFile(filePath) {
    const fs = require('fs');
    fs.readFile(filePath, 'utf-8', (err, rawData) => {
        if(err){
            alert("An error ocurred reading the file :" + err.message);
            return;
        }
        const netwJsn = JSON.parse(rawData);
        const viewerJsn = 

        eNetworkViewer.loadEJson(netwJsn);
    });
}

ipcRenderer.on('open-file', (event, filePath) => {
    openFile(filePath);
});

contextBridge.exposeInMainWorld('api', {
    eNetworkViewer: () => {return eNetworkViewer;},
});

// vim: ts=4 sw=4:
