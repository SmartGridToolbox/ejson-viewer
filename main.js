// Modules to control application life and create native browser window
const {app, BrowserWindow, dialog, Menu, session} = require('electron');
const fs = require('fs');
const path = require('path');

const isMac = process.platform === 'darwin';

function createWindow () {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        width: 1200,
        height: 900,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'), // Security, nodeIntegration = false by default.
            contextIsolation: true, // Protect against prototype pollution.
            enableRemoteModule: false, // Turn off remote.
            worldSafeExecuteJavaScript: true
        }
    });

    // and load the index.html of the app.
    mainWindow.loadFile('index.html');

    // Open the DevTools.
    // mainWindow.webContents.openDevTools();
}

const menuTemplate = [
    // {role: 'appMenu'}
    ...(isMac ? [{
        label: app.name,
        submenu: [
            {role: 'about'},
            {type: 'separator'},
            {role: 'services'},
            {type: 'separator'},
            {role: 'hide'},
            {role: 'hideothers'},
            {role: 'unhide'},
            {type: 'separator'},
            {role: 'quit'}
        ]
    }] : []),
    // {role: 'fileMenu'}
    {
        label: 'File',
        submenu: [
            {label: 'Open', role: 'open', click: openFileDialog},
            {label: 'Close', role: 'close'}
        ]
    },
      // { role: 'editMenu' }
    {
        label: 'Edit',
        submenu: [
            { role: 'undo' },
            { role: 'redo' },
            { type: 'separator' },
            { role: 'cut' },
            { role: 'copy' },
            { role: 'paste' },
            ...(isMac ? [
                { role: 'pasteAndMatchStyle' },
                { role: 'delete' },
                { role: 'selectAll' },
                { type: 'separator' },
                {
                    label: 'Speech',
                    submenu: [
                        { role: 'startspeaking' },
                        { role: 'stopspeaking' }
                    ]
                }
            ] : [
                { role: 'delete' },
                { type: 'separator' },
                { role: 'selectAll' }
            ])
        ]
    },
    // { role: 'viewMenu' }
    {
        label: 'View',
        submenu: [
            { role: 'reload' },
            { role: 'forcereload' },
            { role: 'toggledevtools' },
            { type: 'separator' },
            { role: 'resetzoom' },
            { role: 'zoomin' },
            { role: 'zoomout' },
            { type: 'separator' },
            { role: 'togglefullscreen' }
        ]
    },
];

const menu = Menu.buildFromTemplate(menuTemplate);
Menu.setApplicationMenu(menu);

app.on('web-contents-created', (event, contents) => {
    contents.on('will-attach-webview', (event, webPreferences, params) => {
        // Strip away preload scripts if unused or verify their location is legitimate
        delete webPreferences.preload
        delete webPreferences.preloadURL

        // Disable Node.js integration
        webPreferences.nodeIntegration = false

        // Verify URL being loaded
        // if (!params.src.startsWith('https://example.com/')) {
        event.preventDefault()
        // }
    })
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
    const { session } = require('electron')
    session.defaultSession.setPermissionRequestHandler((webContents, permission, callback) => {
        const url = webContents.getURL();
        /*
        if (permission === 'notifications') {
        // Approves the permissions request
            callback(true)
        }

    // Verify URL
        if (!url.startsWith('https://example.com/')) {
    // Denies the permissions request
            return callback(false)
        }
        */
    return callback(false);
});

createWindow();

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
    });
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
    if (!isMac) app.quit();
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

function openFileDialog() {
    dialog.showOpenDialog({properties: ['openFile', 'multiSelections']}).then((result) => {
        if (!result.canceled) {
            const filePath = result.filePaths[0];
            BrowserWindow.getFocusedWindow().webContents.send('open-file', filePath);
        }
    });
}

// vim: ts=4 sw=4:
